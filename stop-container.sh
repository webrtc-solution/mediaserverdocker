#!/bin/sh

dockerdocker=`docker ps | grep docker | grep -v grep | awk '{print$1}'`
if [ ! -z $dockerdocker ]
then
echo "Terminating docker container docker with ID: " $dockerdocker
docker exec -it $dockerdocker bash -c "${ENTRY_POINT}"
docker stop $dockerdocker
fi